
#include<Wire.h>
#include <Adafruit_BMP085.h>
#include <SoftwareSerial.h>
#include <MPU6050.h>
#define BT_TX_PIN 12
#define BT_RX_PIN 11


SoftwareSerial bt =  SoftwareSerial(BT_RX_PIN, BT_TX_PIN);
String x, z, t, a, p;

Adafruit_BMP085 bmp;
MPU6050 mpu;
String incChar;
void setup() {
  Serial.begin(9600);
  if (!bmp.begin()) {
    Serial.print(-1);
  }
  if (!mpu.begin(MPU6050_SCALE_2000DPS, MPU6050_RANGE_2G))
  {
    Serial.println("-2");
  }
}

void loop() {
  if (Serial.read() == 82) {
    Serial.flush();
    Vector normAccel = mpu.readNormalizeAccel();
    x = (String)(-(atan2(normAccel.XAxis, sqrt(normAccel.YAxis * normAccel.YAxis + normAccel.ZAxis * normAccel.ZAxis)) * 180.0) / M_PI);
    z = (String)((atan2(normAccel.YAxis, normAccel.ZAxis) * 180.0) / M_PI);
    t = (String)bmp.readTemperature();
    a = (String)bmp.readAltitude();
    p = (String)(bmp.readPressure() / 1000);
    /*
      Serial.println("X = "+x);
      Serial.println("Z = "+z);
      Serial.println("Temp = "+t);
      Serial.println("Altitud = "+a);
      Serial.println("Pressure = "+p);
    */
    Serial.print((String)x + ":" + (String)z + ":" + (String)t + ":" + (String)p + ":" + (String)a + ":" + "F");
  }
}
